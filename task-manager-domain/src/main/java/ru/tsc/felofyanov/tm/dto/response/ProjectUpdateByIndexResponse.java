package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }
}
