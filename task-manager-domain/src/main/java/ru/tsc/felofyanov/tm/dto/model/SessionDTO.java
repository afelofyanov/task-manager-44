package ru.tsc.felofyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class SessionDTO extends AbstractUserOwnerModelDTO {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;
}
