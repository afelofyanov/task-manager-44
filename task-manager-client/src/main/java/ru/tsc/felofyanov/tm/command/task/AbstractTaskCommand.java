package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void renderTasks(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        System.out.println("[INDEX.NAME]-------------------[ID]--------------------[STATUS]--------[DATE BEGIN]");
        for (final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

    public void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        @NotNull final Status status = task.getStatus();
        System.out.println("STATUS: " + status.getDisplayName());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }
}
