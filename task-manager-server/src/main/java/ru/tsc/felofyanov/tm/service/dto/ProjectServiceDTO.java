package ru.tsc.felofyanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;
import ru.tsc.felofyanov.tm.repository.dto.ProjectRepositoryDTO;

import javax.persistence.EntityManager;

public class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO>
        implements IProjectServiceDTO {

    public ProjectServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryDTO(entityManager);
    }
}
