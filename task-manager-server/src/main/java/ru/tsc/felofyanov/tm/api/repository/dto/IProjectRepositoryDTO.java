package ru.tsc.felofyanov.tm.api.repository.dto;

import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnerRepositoryDTO<ProjectDTO> {

}
