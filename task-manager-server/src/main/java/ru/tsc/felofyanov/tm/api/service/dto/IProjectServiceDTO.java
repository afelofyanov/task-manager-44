package ru.tsc.felofyanov.tm.api.service.dto;

import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnerServiceDTO<ProjectDTO> {

}
