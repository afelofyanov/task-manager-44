package ru.tsc.felofyanov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskServiceDTO {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);
}
