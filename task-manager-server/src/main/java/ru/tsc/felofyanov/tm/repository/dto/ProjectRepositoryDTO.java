package ru.tsc.felofyanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public class ProjectRepositoryDTO extends AbstractUserOwnerRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager, ProjectDTO.class);
    }
}
