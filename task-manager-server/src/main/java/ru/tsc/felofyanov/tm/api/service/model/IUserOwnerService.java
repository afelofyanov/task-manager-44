package ru.tsc.felofyanov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.AbstractWbs;

public interface IUserOwnerService<M extends AbstractWbs> extends IUserOwnerRepository<M>, IService<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    M updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);
}
